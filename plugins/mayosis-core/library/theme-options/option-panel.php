<?php
    
include_once(dirname( __FILE__ ).'/global-colors/global-style.php'); 
include_once(dirname( __FILE__ ).'/global-colors/footer-color.php');  
include_once(dirname( __FILE__ ).'/global-colors/thumbnail-color.php'); 
include_once(dirname( __FILE__ ).'/global-colors/blog-style.php'); 
include_once(dirname( __FILE__ ).'/global-colors/widget-color.php'); 
include_once(dirname( __FILE__ ).'/global-colors/page-builder-color.php'); 
include_once(dirname( __FILE__ ).'/global-colors/button-style.php'); 
include_once(dirname( __FILE__ ).'/template/tag-style.php'); 
include_once(dirname( __FILE__ ).'/footer/footer-options.php'); 
include_once(dirname( __FILE__ ).'/product-options/product-options.php'); 
include_once(dirname( __FILE__ ).'/template/product-template.php'); 
include_once(dirname( __FILE__ ).'/template/other-template.php'); 
include_once(dirname( __FILE__ ).'/typography/typography.php'); 
include_once(dirname( __FILE__ ).'/white-label/white-label.php'); 
include_once(dirname( __FILE__ ).'/other-options/other-options.php'); 