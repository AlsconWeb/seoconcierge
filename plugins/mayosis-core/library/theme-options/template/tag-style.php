 <?php //Start tag template
Mayosis_Option::add_field( 'mayo_config', array(
    'type'        => 'select',
        'settings'    => 'tag_bg_type',
        'label'       => __( 'Tag Breadcrumb Background Type', 'mayosis' ),
        'section'     => 'tag_style',
        'default'     => 'gradient',
        'priority'    => 10,
        'choices'     => array(
            'color'  => esc_attr__( 'Color', 'mayosis' ),
            'gradient' => esc_attr__( 'Gradient', 'mayosis' ),
        ),
        
    
) );

Mayosis_Option::add_field( 'mayo_config', array(
    'type'        => 'color',
        'settings'     => 'tag_background',
        'label'       => __( 'Breadcrumb Background Color', 'mayosis' ),
        'description' => __( 'Set Breadcrumb Background color', 'mayosis' ),
        'section'     => 'tag_style',
        'priority'    => 10,
        'default'     => '#1e0047',
        'output' => array(
        	array(
        		'element'  => '.tag_breadcrumb_color',
        		'property' => 'background',
        	),
),
        'required'    => array(
            array(
                'setting'  => 'tag_bg_type',
                'operator' => '==',
                'value'    => 'color',
            ),
        ),
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
        
    
) );

Mayosis_Option::add_field( 'mayo_config', array(
    'type'        => 'multicolor',
        'settings'    => 'tag_gradient',
        'label'       => esc_attr__( 'Breadcrumb gradient', 'mayosis' ),
        'section'     => 'tag_style',
        'priority'    => 10,
        'required'    => array(
            array(
                'setting'  => 'tag_bg_type',
                'operator' => '==',
                'value'    => 'gradient',
            ),
        ),
        'choices'     => array(
            'color1'    => esc_attr__( 'Form', 'mayosis' ),
            'color2'   => esc_attr__( 'To', 'mayosis' ),
        ),
        'default'     => array(
            'color1'    => '#1e0046',
            'color2'   => '#1e0064',
        ),
        
    
) );

Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'typography',
	'settings'    => 'tag_heading_typo',
	'label'       => esc_html__( 'Tag heading Typo', 'kirki' ),
	'section'     => 'tag_style',
	'default'     => [
		'font-family'    => 'Lato',
		'variant'        => '700',
		'font-size'      => '36px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#ffffff',
		'text-transform' => 'none',
		'text-align'     => 'center',
	],
	'priority'    => 10,
	'transport'   => 'auto',
	'output'      => [
		[
			'element' => '.tag_breadcrumb_color .parchive-page-title',
		],
	],
] );