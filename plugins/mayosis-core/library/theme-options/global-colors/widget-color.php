<?php

Mayosis_Option::add_field( 'mayo_config', array(
            'type'        => 'radio-buttonset',
            'settings'    => 'wd_bg_type',
            'label'       => __( 'Title Background Type', 'mayosis' ),
            'section'     => 'widget_color',
            'default'     => 'color',
            'priority'    => 10,
            'choices'     => array(
                'color'  => esc_attr__( 'Color', 'mayosis' ),
                'gradient' => esc_attr__( 'Gradient', 'mayosis' ),
            ),
));

Mayosis_Option::add_field( 'mayo_config', array(
        'type'        => 'multicolor',
        'settings'    => 'wd_title_gradient',
        'label'       => esc_attr__( 'Widget gradient', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'required'    => array(
            array(
                'setting'  => 'wd_bg_type',
                'operator' => '==',
                'value'    => 'gradient',
            ),
        ),
        'choices'     => array(
            'color1'    => esc_attr__( 'Form', 'mayosis' ),
            'color2'   => esc_attr__( 'To', 'mayosis' ),
        ),
        'default'     => array(
            'color1'    => '#1e73be',
            'color2'   => '#00897e',
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
        'type'        => 'color',
        'settings'     => 'wd_title_bg',
        'label'       => __( 'Title Background Color', 'mayosis' ),
        'description' => __( 'Set Title Background color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#1e0046',
        'required'    => array(
            array(
                'setting'  => 'wd_bg_type',
                'operator' => '==',
                'value'    => 'color',
            ),
        ),
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
));



Mayosis_Option::add_field( 'mayo_config', array(
       'type'        => 'color',
        'settings'     => 'wd_title_text',
        'label'       => __( 'Title Color', 'mayosis' ),
        'description' => __( 'Set Title color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#ffffff',
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
        'type'        => 'color',
        'settings'     => 'theme_widget_bg_color_main',
        'label'       => __( 'Widget Background Color', 'mayosis' ),
        'description' => __( 'Change Widget Backgrounnd Color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#e9edf7',
        'output' => array(
            	array(
            		'element'  => '.theme--sidebar--widget,.subscribe-box-photo,.sidebar-theme',
            		'property' => 'background',
            	),
            	

    	),
    	
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
    ));
    
Mayosis_Option::add_field( 'mayo_config', array(
        'type'        => 'color',
        'settings'     => 'wd_field_text',
        'label'       => __( 'Field Text Color', 'mayosis' ),
        'description' => __( 'Set Field Text color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#28375a',
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
       'type'        => 'radio-buttonset',
        'settings'    => 'wd_field_type',
        'label'       => __( 'Form Field Type', 'mayosis' ),
        'section'     => 'widget_color',
        'default'     => 'solid',
        'priority'    => 10,
        'choices'     => array(
            'solid'  => esc_attr__( 'Solid', 'mayosis' ),
            'border' => esc_attr__( 'Border', 'mayosis' ),
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
       'type'        => 'color',
        'settings'     => 'wd_field_color',
        'label'       => __( 'Form Field Color', 'mayosis' ),
        'description' => __( 'Change Form Field Color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#edeff2',
        'required'    => array(
            array(
                'setting'  => 'wd_field_type',
                'operator' => '==',
                'value'    => 'solid',
            ),
        ),
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
       'type'        => 'color',
        'settings'     => 'wd_border_color',
        'label'       => __( 'Form Border Color', 'mayosis' ),
        'description' => __( 'Change Form border Color', 'mayosis' ),
        'section'     => 'widget_color',
        'priority'    => 10,
        'default'     => '#282837',
        'required'    => array(
            array(
                'setting'  => 'wd_field_type',
                'operator' => '==',
                'value'    => 'border',
            ),
        ),
        'choices' => array(
            'palettes' => array(
                '#28375a',
                '#282837',
                '#5a00f0',
                '#ff6b6b',
                '#c44d58',
                '#ecca2e',
                '#bada55',
            ),
        ),
));

Mayosis_Option::add_field( 'mayo_config', array(
        'type'        => 'dimension',
        'settings'    => 'wd_border_thikness',
        'label'       => esc_attr__( 'Border Thickness', 'mayosis' ),
        'description' => esc_attr__( 'Add Main Site Form Border Thickness', 'mayosis' ),
        'section'     => 'widget_color',
        'default'     => '2px',
        'required'    => array(
            array(
                'setting'  => 'wd_field_type',
                'operator' => '==',
                'value'    => 'border',
            ),
        ),
));

  Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'dimensions',
	'settings'    => 'sidebar_border_radius',
	'section'     => 'widget_color',
	'label'       => esc_attr__( 'Border Radius', 'mayosis' ),
	'default'     => [
		'top-left-radius'     => '3px',
		'top-right-radius'    => '3px',
		'bottom-left-radius'  => '3px',
		'bottom-right-radius' => '3px',
	],
	'choices'     => [
		'top-left-radius'     => esc_attr__( 'Top Left', 'mayosis' ),
		'top-right-radius'    => esc_attr__( 'Top Right', 'mayosis' ),
		'bottom-left-radius'  => esc_attr__( 'Bottom Left', 'mayosis' ),
		'bottom-right-radius' => esc_attr__( 'Bottom Right', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'border',
			'element'  => '.theme--sidebar--widget,.subscribe-box-photo',
		],
	]
] );

 Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'dimensions',
	'settings'    => 'sidebar_border_radius_title',
	'section'     => 'widget_color',
	'label'       => esc_attr__( 'Title Border Radius', 'mayosis' ),
	'default'     => [
		'top-left-radius'     => '3px',
		'top-right-radius'    => '3px',
		'bottom-left-radius'  => '0px',
		'bottom-right-radius' => '0px',
	],
	'choices'     => [
		'top-left-radius'     => esc_attr__( 'Top Left', 'mayosis' ),
		'top-right-radius'    => esc_attr__( 'Top Right', 'mayosis' ),
		'bottom-left-radius'  => esc_attr__( 'Bottom Left', 'mayosis' ),
		'bottom-right-radius' => esc_attr__( 'Bottom Right', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'border',
			'element'  => '.theme--sidebar--widget .widget-title',
		],
	]
] );

 Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'dimensions',
	'settings'    => 'sidebar_box_padding_main',
	'section'     => 'widget_color',
	'label'       => esc_attr__( 'Widget Box Padding', 'mayosis' ),
	'default'     => [
		'top'     => '10px',
		'right'    => '30px',
		'bottom'  => '10px',
		'left' => '30px',
	],
	'choices'     => [
		'top'     => esc_attr__( 'Top', 'mayosis' ),
		'right'    => esc_attr__( 'Right', 'mayosis' ),
		'bottoms'  => esc_attr__( 'Bottom', 'mayosis' ),
		'left' => esc_attr__( 'Left', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'padding',
			'element'  => '.theme--sidebar--widget',
		],
	]
] );

 Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'dimensions',
	'settings'    => 'sidebar_box_title_margin',
	'section'     => 'widget_color',
	'label'       => esc_attr__( 'Widget Title Margin', 'mayosis' ),
	'default'     => [
		'top'     => '-10px',
		'right'    => '-30px',
		'bottom'  => '20px',
		'left' => '-30px',
	],
	'choices'     => [
		'top'     => esc_attr__( 'Top', 'mayosis' ),
		'right'    => esc_attr__( 'Right', 'mayosis' ),
		'bottoms'  => esc_attr__( 'Bottom', 'mayosis' ),
		'left' => esc_attr__( 'Left', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'margin',
			'element'  => '.theme--sidebar--widget .widget-title',
		],
	]
] );

Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'dimensions',
	'settings'    => 'sidebar_box_title_padding',
	'section'     => 'widget_color',
	'label'       => esc_attr__( 'Widget Title Padding', 'mayosis' ),
	'default'     => [
		'top'     => '25px',
		'right'    => '25px',
		'bottom'  => '25px',
		'left' => '25px',
	],
	'choices'     => [
		'top'     => esc_attr__( 'Top', 'mayosis' ),
		'right'    => esc_attr__( 'Right', 'mayosis' ),
		'bottoms'  => esc_attr__( 'Bottom', 'mayosis' ),
		'left' => esc_attr__( 'Left', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'padding',
			'element'  => '.theme--sidebar--widget .widget-title',
		],
	]
] );

Mayosis_Option::add_field( 'mayo_config', [
	'type'        => 'radio-buttonset',
	'settings'    => 'hide_widget_title',
	'label'       => esc_html__( 'Hide/Show Widget Title', 'mayosis' ),
	'section'     => 'widget_color',
	'default'     => 'block',
	'priority'    => 10,
	'choices'     => [
		'none'   => esc_html__( 'Hide', 'mayosis' ),
		'block' => esc_html__( 'Show', 'mayosis' ),
	],
	'transport'   => 'auto',
	'output'    => [
		[
			'property' => 'display',
			'element'  => '.theme--sidebar--widget .widget-title',
		],
	]
] );

