<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class mayosis_edd_category_Elementor extends Widget_Base {

    public function get_name() {
        return 'mayosis-edd-category';
    }

    public function get_title() {
        return __( 'Mayosis Download Category', 'mayosis' );
    }
    public function get_categories() {
        return [ 'mayosis-ele-cat' ];
    }
    public function get_icon() {
        return 'eicon-elementor';
    }

    protected function _register_controls() {

        $this->add_control(
            'section_edd',
            [
                'label' => __( 'Mayosis Download Category', 'mayosis' ),
                'type' => Controls_Manager::SECTION,
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'mayosis' ),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'title' => __( 'Enter Title', 'mayosis' ),
                'section' => 'section_edd',
            ]
        );

       
        
        $this->add_control(
            'custom_css',
            [
                'label' => __( 'Custom CSS', 'mayosis' ),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'title' => __( 'Enter Custom CSS name', 'mayosis' ),
                'section' => 'section_edd',
            ]
        );
        $this->add_control(
            'catstyle',
            [
                'label' => __( 'Category Style', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'grid' => 'Grid',
                    'list' => 'List'
                ],
                'default' => 'grid',

            ]
        );
        
        $this->add_control(
			'number_of_category',
			[
				'label' => __( 'Number of Category', 'mayosis' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'section' => 'section_edd',
				'default' => __( '', 'mayosis' ),
				'placeholder' => __( 'Input Number of Category', 'mayosis' ),
			]
		);
		
		$this->add_control(
            'showcatwise',
            [
                'label' => __( 'Category Type', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'all' => 'All',
                    'parent' => 'Parent Category Wise'
                ],
                'default' => 'all',

            ]
        );
        
        $this->add_control(
			'parent_cat_slug',
			[
				'label' => __( 'Category ID', 'mayosis' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'section' => 'section_edd',
				'default' => __( '', 'mayosis' ),
				'placeholder' => __( 'Parent Category ID', 'mayosis' ),
				'condition' => [
                    'showcatwise' => array('parent'),
                ],
			]
		);
        $this->add_control(
            'carousel',
            [
                'label' => __( 'Carousel', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                ],
                'default' => 'disable',
                 'condition' => [
                    'catstyle' => array('grid'),
                ],

            ]
        );
        	$this->start_controls_section(
			'other_style',
			[
				'label' => __( 'Style', 'mayosis' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typo',
				'label' => __( 'Title Typography', 'mayosis' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .section-title',
			]
		);
		$this->add_control(
            'list-col',
            [
                'label' => __( 'List Style Column', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'other_style',
                'options' => [
                    '1' => 'One',
                    '2' => 'Two',
                    '3' => 'Three',
                    '4' => 'Four',
                    '5' => 'Five',
                    '6' => 'Six',
                    
                ],
                'default' => '3',
                 'condition' => [
                    'catstyle' => array('list'),
                ],

            ]
        );
        
        $this->add_control(
         'list-style-text',
         [
            'label' => __( 'List Style Item Color', 'mayosis' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#54595f',
            'title' => __( 'Select Item Color', 'mayosis' ),
            'selectors' => [
					'{{WRAPPER}} .list-download-cat a' => 'color: {{VALUE}}',
					],
					'condition' => [
                    'catstyle' => array('list'),
                ],
            
         ]
      );
      
       $this->add_control(
         'list-style-text-hover',
         [
            'label' => __( 'List Style Item Hover Color', 'mayosis' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#4054b2',
            'title' => __( 'Select Item Hover Color', 'mayosis' ),
            'selectors' => [
					'{{WRAPPER}} .list-download-cat a:hover' => 'color: {{VALUE}}',
					],
					'condition' => [
                    'catstyle' => array('list'),
                ],
            
         ]
      );
	
$this->end_controls_section();
    }

    protected function render( $instance = [] ) {

        // get our input from the widget settings.

        $settings = $this->get_settings();
        $custom_css = $settings['custom_css'];
        $recent_section_title = $settings['title'];
        $catstyle = $settings['catstyle'];
        $carousel_enable = $settings['carousel'];
        $listcol = $settings['list-col'];
        $amount=$settings['number_of_category'];
        $showcid=$settings['showcatwise'];
        $pid=$settings['parent_cat_slug'];
      
        ?>

   
        <div class="<?php
        echo esc_attr($custom_css); ?>">
             <h2 class="section-title"><?php echo esc_attr($recent_section_title); ?> </h2>
              <?php if ($catstyle=='list'){ ?>
              <div class="grid--download--list col-count-<?php echo $listcol; ?>">
                  <?php if($showcid=='parent'){?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category','parent' => $pid, 'number' => $amount )
); ?>
                  <?php } else{ ?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category', 'number' => $amount )
); ?>

<?php } ?>
              <?php foreach ( $categories as $term ) : ?>
<?php $category_grid_image = get_term_meta( $term->term_id, 'category_image_main', true); ?>
	<div class="list-download-cat">
		<a href="<?php echo esc_attr( get_term_link( $term, $taxonomy ) ); ?>" title="<?php echo $term->name; ?>"><span><?php echo $term->name; ?></span></a>
</div>
<?php endforeach; ?>
               </div>
              <?php }  else { ?>
              
             <?php if ($carousel_enable=='enable'){ ?>
              
              <?php if($showcid=='parent'){?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category','parent' => $pid, 'number' => $amount )
); ?>
                  <?php } else{ ?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category', 'number' => $amount )
); ?>

<?php } ?>

<div class="edd-category-grid-carousel">
  
         <div id="grid-cat-edd">
<?php foreach ( $categories as $term ) : ?>
<?php $category_grid_image = get_term_meta( $term->term_id, 'category_image_main', true); ?>
		<a href="<?php echo esc_attr( get_term_link( $term, $taxonomy ) ); ?>" title="<?php echo $term->name; ?>"  class="cat--grid--main">
		    <div class="edd-cat-box-main"><span><?php echo $term->name; ?></span>
		    <div class="edd-cat-overlay-img" style="background:url(<?php echo $category_grid_image; ?>)">
		        </div>
	
		</div></a>

<?php endforeach; ?>
</div>
</div>
              
              
             <?php } else { ?>
              
              <?php if($showcid=='parent'){?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category','parent' => $pid, 'number' => $amount )
); ?>
                  <?php } else{ ?>
                  <?php $categories=get_categories(
    array( 'taxonomy' => 'download_category', 'number' => $amount )
); ?>

<?php } ?>

<div class="grid--download--categories">
<?php foreach ( $categories as $term ) : ?>
<?php $category_grid_image = get_term_meta( $term->term_id, 'category_image_main', true); ?>
	
		<a href="<?php echo esc_attr( get_term_link( $term, $taxonomy ) ); ?>" title="<?php echo $term->name; ?>" style="background:url(<?php echo $category_grid_image; ?>)" class="cat--grid--main"><span><?php echo $term->name; ?></span></a>

<?php endforeach; ?>
</div>
             <?php } ?>
               <?php } ?>
       
        </div>


        <?php

    }

    protected function content_template() {}

    public function render_plain_content( $instance = [] ) {}

}
Plugin::instance()->widgets_manager->register_widget_type( new mayosis_edd_category_Elementor );
?>