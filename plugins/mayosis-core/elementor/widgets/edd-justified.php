<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class mayosis_edd_justified_Elementor extends Widget_Base {

    public function get_name() {
        return 'mayosis-edd-justified';
    }

    public function get_title() {
        return __( 'Mayosis EDD Justified Grid', 'mayosis' );
    }
    public function get_categories() {
        return [ 'mayosis-ele-cat' ];
    }
    public function get_icon() {
        return 'eicon-elementor';
    }

    protected function _register_controls() {

        $this->add_control(
            'section_edd',
            [
                'label' => __( 'Mayosis EDD Justified', 'mayosis' ),
                'type' => Controls_Manager::SECTION,
            ]
        );

        

        $this->add_control(
            'item_per_page',
            [
                'label'   => esc_html_x( 'Amount of item to display', 'Admin Panel', 'mayosis' ),
                'type'    => Controls_Manager::NUMBER,
                'default' =>  "10",
                'section' => 'section_edd',
            ]
        );
        
       
        $this->add_control(
            'show_category',
            [
                'label'     => esc_html_x( 'Filter Category Wise', 'Admin Panel','mayosis' ),
                'description' => esc_html_x('Select if want to show product by category', 'mayosis' ),
                'type'      =>  Controls_Manager::SELECT,
                'default'    =>  "no",
                'section' => 'section_edd',
                "options"    => array(
                    "yes" => "Yes",
                    "no" => "No",

                ),
            ]

        );


        $this->add_control(
            'category',
            [
                'label' => __( 'Category Slug', 'mayosis' ),
                'description' => __('Add one category slug','mayosis'),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'section' => 'section_edd',
            ]
        );
        $this->add_control(
            'filter-product',
            [
                'label' => __( 'Show Product Filter', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'show' => 'Show',
                    'hide' => 'Hide'
                ],
                'default' => 'hide',

            ]
        );
        
        $this->add_control(
            'order',
            [
                'label' => __( 'Order', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'default' => 'desc',

            ]
        );
         $this->add_control(
            'titilebox',
            [
                'label' => __( 'Title Hover Box', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'show' => 'Show',
                    'hide' => 'Hide'
                ],
                'default' => 'hide',

            ]
        );
        
        $this->add_control(
            'titileboxstyle',
            [
                'label' => __( 'Title Box Style', 'mayosis' ),
                'type' => Controls_Manager::SELECT,
                'section' => 'section_edd',
                'options' => [
                    'one' => 'One',
                    'two' => 'Two',
                    'three' => 'three'
                ],
                'default' => 'one',

            ]
        );
        
        $this->add_control(
            'grid_gap',
            [
                'label' => __( 'Grid Gap', 'mayosis' ),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'title' => __( 'Enter Grid gap Without PX', 'mayosis' ),
                'section' => 'section_edd',
                'default' => '2.5',
            ]
        );
        
        $this->add_control(
            'custom_css',
            [
                'label' => __( 'Custom CSS', 'mayosis' ),
                'type' => Controls_Manager::TEXT,
                'default' => '',
                'title' => __( 'Enter Custom CSS name', 'mayosis' ),
                'section' => 'section_edd',
            ]
        );
    $this->start_controls_section(
			'other_style',
			[
				'label' => __( 'Style', 'mayosis' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typo',
				'label' => __( 'Title Typography', 'mayosis' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .section-title',
			]
		);
	
$this->end_controls_section();
    }

    protected function render( $instance = [] ) {

        // get our input from the widget settings.

        $settings = $this->get_settings();
        $post_count = ! empty( $settings['item_per_page'] ) ? (int)$settings['item_per_page'] : 5;
        $post_order_term=$settings['order'];
        $downloads_category=$settings['category'];
        $filterproduct = $settings['filter-product'];
        $custom_css = $settings['custom_css'];
        $grid_gap= $settings['grid_gap'];
        $titlebox = $settings['titilebox'];
        $titileboxstyle = $settings['titileboxstyle'];
        ?>

   
        <div class="<?php
        echo esc_attr($custom_css); ?>">
            <div class="row">
                <div class="col-md-12">
                    <?php  if( $filterproduct == 'show' ) : ?>
                         <div id="justifiedfiltercontrol">
                            <button value="*"><?php esc_html_e('All Categories','mayosis'); ?></button>
                            <?php

                            $taxonomy = 'download_category';
                            $terms = get_terms($taxonomy); // Get all terms of a taxonomy

                            if ( $terms && !is_wp_error( $terms ) ) :
                                ?>

                                <?php foreach ( $terms as $term ) { ?>
                                <button value=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>
                            <?php } ?>

                            <?php endif;?>
                        </div>
                    <?php endif;?>


                    <div class="gridzy justified-gallery-main gridzyLightProgressIndicator gridzyAnimated" data-gridzy-spaceBetween="<?php echo esc_html($grid_gap);?>" data-gridzy-filterControls="#justifiedfiltercontrol button">

        <?php
        global $post;
        if($settings['show_category'] == 'no') {
            $args = array('post_type' => 'download', 'numberposts' => $post_count, 'order' => (string)trim($post_order_term),);
        } else {
            $args = array(
                'post_type' => 'download',
                'numberposts' => $post_count,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'download_category',
                        'field' => 'slug',
                        'terms' => $downloads_category,
                    ),
                ),
                'order' => (string)trim($post_order_term),);
        }
        $recent_posts = get_posts( $args );
        foreach( $recent_posts as $post ){?>
            <?php
            global $post;
            $downlodterms = get_the_terms( $post->ID, 'download_category' );// Get all terms of a taxonomy
            $cls = '';

            if ( ! empty( $downlodterms ) ) {
                foreach ($downlodterms as $term ) {
                    $cls .= $term->slug . ' ';
                }
            }
            ?>
                        <div class="justified-items <?php echo $cls; ?> ">
                            <div class="product-justify-item-content">
                                
                                    <?php if ( has_post_format( 'video' )) { ?>
                                        <div class="item-thumbnail item-video-justify">
                                            <?php get_template_part( 'library/mayosis-video-box-thumb' ); ?>
                                        </div>
                                    <?php } else { ?>
                                    <?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'large');?>
                                    <div class="item-thumbnail">
                                    <a href="<?php the_permalink();?>"><img src="<?php echo $thumbnail['0']; ?>" alt=""></a>
                                     </div>
                                    <?php } ?>
                                
                                <?php if ($titlebox=="show"){?>
                                
                                <?php if ($titileboxstyle== "one"){ ?>
                                <div class="product-justify-description">
                                    
                                    <h5><a href="<?php the_permalink();?>" ><?php the_title()?></a></h5>
                                    </div>
                                    
                                <?php } elseif ($titileboxstyle== "three"){ ?>
                                
                                 <div class="product-justify-description justify-style-three">
                                     <div class="product_hover_details_button">
                                  <a href="<?php the_permalink();?>"  class="button-fill-color"><?php esc_html_e('View Details','mayosis');?></a>
                                </div>
                                    
                                    </div>
                                <?php } else { ?>
                                <div class="product-justify-description justify-style-two">
                                    
                                    <h5><a href="<?php the_permalink();?>" ><?php the_title()?></a></h5>
                                    
                                    <div class="bottom-metaflex">
                                    <?php if ( function_exists( 'edd_favorites_load_link' ) ) {
                        edd_favorites_load_link( $download_id );
                    } ?> <span> <a href="<?php echo mayosis_fes_author_url( get_the_author_meta( 'ID',$author_id ) ) ?>">
								     
								     <i class="zil zi-user"></i>
								 </a></span>
								 </div>
                                </div>
                                <?php } ?>
                                <?php } ?>
                                
                                <?php if ($bottommetabox=="show"){?>
                                <div class="product-meta">
                                <?php get_template_part( 'includes/product-meta' ); ?>
                            </div>
                            <?php } ?>
                            
                            
                            </div>
                            
                        </div>

        <?php } ?>



                        <div class="clearfix"></div>
                        <?php  wp_reset_postdata();
                        ?>




                </div>
                </div>
            </div>
        </div>


        <?php

    }

    protected function content_template() {}

    public function render_plain_content( $instance = [] ) {}

}
Plugin::instance()->widgets_manager->register_widget_type( new mayosis_edd_justified_Elementor );
?>