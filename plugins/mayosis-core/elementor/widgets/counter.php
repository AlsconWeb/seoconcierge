<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Counter_Elementor_Thing extends Widget_Base {

   public function get_name() {
      return 'mayosis-counter';
   }

   public function get_title() {
      return __( 'Mayosis Stats Counter', 'mayosis' );
   }
public function get_categories() {
		return [ 'mayosis-ele-cat' ];
	}
   public function get_icon() { 
        return 'eicon-counter-circle';
   }

   protected function _register_controls() {

      $this->add_control(
         'counter_settings',
         [
            'label' => __( 'Counter Settings', 'mayosis' ),
            'type' => Controls_Manager::SECTION,
         ]
      );

       $this->add_control(
         'title',
         [
            'label' => __( 'Counter Title', 'mayosis' ),
            'type' => Controls_Manager::TEXT,
            'default' => 'Title',
            'section' => 'counter_settings',
         ]
      );
       
       $this->add_control(
         'counter_type',
         [
            'label' => __( 'Counter Type', 'mayosis' ),
            'type' => Controls_Manager::SELECT,
            'default' => 'user',
            'title' => __( 'Select Counter Type', 'mayosis' ),
            'section' => 'counter_settings',
             'options' => [
                    'user'  => __( 'Total User', 'mayosis' ),
                    'product' => __( 'Total Products', 'mayosis' ),
                    'download' => __( 'Total Download', 'mayosis' ),
                    'custom' => __( 'Custom', 'mayosis' ),
                 ],
         ]
      );
       
       $this->add_control(
         'count',
         [
            'label' => __( 'Custom Count', 'mayosis' ),
            'type' => Controls_Manager::TEXT,
            'default' => '4515',
            'section' => 'counter_settings',
         ]
      );
       
       $this->add_control(
         'count_suffix',
         [
            'label' => __( 'Counter Suffix', 'mayosis' ),
            'type' => Controls_Manager::TEXT,
            'default' => '',
            'section' => 'counter_settings',
         ]
      );
       	$this->start_controls_section(
			'counter_style',
			[
				'label' => __( 'Counter Style', 'mayosis' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_responsive_control(
			'align_text',
			[
				'label' => __( 'Alignment', 'mayosis' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'mayosis' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'mayosis' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'mayosis' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .counter-box' => 'text-align: {{VALUE}};',
				],
			]
		);
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'counter_typo',
				'label' => __( 'Counter Typography', 'mayosis' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .statistic-counter,{{WRAPPER}}  .counter-suffix',
			]
		);
		
       $this->add_control(
         'count_color',
         [
            'label' => __( 'Color of Count', 'mayosis' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#ffffff',
            'title' => __( 'Select Count Color', 'mayosis' ),
            'selectors' => [
					'{{WRAPPER}} .statistic-counter' => 'color: {{VALUE}};',
				],
         ]
      );
      
       $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typo',
				'label' => __( 'Title Typography', 'mayosis' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mcounter_title_promo',
			]
		);
       $this->add_control(
         'title_color',
         [
            'label' => __( 'Color of Title', 'mayosis' ),
            'type' => Controls_Manager::COLOR,
            'default' => '#ffffff',
            'title' => __( 'Select Title Color', 'mayosis' ),
            'selectors' => [
					'{{WRAPPER}} .mcounter_title_promo' => 'color: {{VALUE}};',
				],
         ]
      );
       
       
       $this->end_controls_section();
   }

   protected function render( $instance = [] ) {

      // get our input from the widget settings.

       $settings = $this->get_settings();
      ?>


        <!-- Element Code start -->
        <div class="counter-box">
               	<?php if($settings['counter_type'] == "user"){ ?>
               	
                	<h4 class="statistic-counter">
                	<?php
                        $result = count_users();
                        echo  $result['total_users'];
                        
                        ?></h4>
                 <p class="mcounter_title_promo"><?php echo $settings['title']; ?></p>
                  <?php } elseif($settings['counter_type'] == "product"){ ?>
                   <?php
			$count_products = wp_count_posts('download');
	$total_products = $count_products->publish;
?>
			<h4 class="statistic-counter"><?php
			echo $total_products; ?></h4>
                 <p class="mcounter_title_promo"><?php echo $settings['title']; ?></p>
                   
                    <?php } elseif($settings['counter_type'] == "download"){ ?>
                     <h4 class="statistic-counter"><?php
			echo edd_count_total_file_downloads(); ?></h4>
                   <p class="mcounter_title_promo"><?php echo $settings['title']; ?></p>
                    <?php
			}
		  else
			{ ?>
			<h4 class="statistic-counter"><?php echo $settings['count']; ?></h4> <span class="counter-suffix"><?php echo $settings['count_suffix']; ?></span>
				   <p class="mcounter_title_promo"><?php echo $settings['title']; ?></p> 
					   <?php
			} ?>
                    
                </div>
      <?php

   }

   protected function content_template() {}

   public function render_plain_content( $instance = [] ) {}

}
Plugin::instance()->widgets_manager->register_widget_type( new Counter_Elementor_Thing );
?>