<?php
/*
*Wave player
*since 3.7.1
*/
global $post;
$mayosis_audio = get_post_meta($post->ID, 'audio_url',true);
$wavecolor=get_theme_mod( 'wave_color','#5a00f0');
 $primaryopcitywave = mayosis_hexto_rgb($wavecolor, 0.25);
?>

                        

 <div class="mayosis_audio_wave_container">
     
     <div class="mayosis-audio-outer controls_<?php the_ID(); ?>">
                                   <div id="play-pause-outer">
                               
                                    
                                    
<button class="ytp-play-button ytp-button" data-action="play" onclick="WavesurferAudio.playPause()" aria-live="assertive" tabindex="32" aria-label="Pause">
   <svg width="40px" height="40px" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <defs>
         <path id="ytp-12" d="M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28">
            <animate id="animation" begin="indefinite" attributeType="XML" attributeName="d" fill="freeze" from="M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28" to="M11,10 L17,10 17,26 11,26 M20,10 L26,10 26,26 20,26" dur="0.1s" keySplines=".4 0 1 1"
            repeatCount="1"></animate>
         </path>
      </defs>
      <use xlink:href="#ytp-12" class="ytp-svg-shadow"></use>
      <use xlink:href="#ytp-12" class="ytp-svg-fill"></use>
   </svg>
</button>
                                
                                 </div>
                                 
                                   </div>
     
                             <div id="WavesurferAudio"></div>
                           

                     
                                
                


                              
                                   
                                   </div>
                                   
                                                       
  <script>

                                var WavesurferAudio = WaveSurfer.create({
                                    container: '#WavesurferAudio',
                                    waveColor: '<?php echo esc_html($primaryopcitywave); ?>',
                                    progressColor: '<?php echo esc_html($wavecolor); ?>',
                                    cursorColor:'<?php echo esc_html($wavecolor); ?>',
                                    height : 165,
                                });
                                WavesurferAudio.load('<?php echo esc_url($mayosis_audio);?>');
                                </script>