<?php
/**
 * Created 20.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php wp_head(); ?>
</head>
<body <?php body_class( 'cart' ); ?>>
<section>
	<div class="page-404"
	     style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/clouds.png), url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/rocket.svg)">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="logo" href="<?php bloginfo( 'url' ); ?>"><img
							src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo-two
				.svg"
							alt="logo"></a>
					<h1>Error 404</h1>
					<p>Sorry but this page went to space <br>or doesn’t exist</p><a class="button" href="<?php bloginfo( 'url' );
					?>">Homepage</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php wp_footer(); ?>
</body>
</html>


