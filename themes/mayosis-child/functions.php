<?php
/**
 * Created 10.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action( 'wp_enqueue_scripts', 'add_script', 40 );
function add_script () {
	wp_enqueue_script( 'google-map', '//maps.googleapis.com/maps/api/js?key=AIzaSyCpVVAoAJDOEP7rJPi-2-P0aBxFuIAxZ5Y&callback=initMap', [ 'jquery' ], '', true );
	wp_enqueue_script( 'build', get_stylesheet_directory_uri() . '/assets/js/build.js', [ 'jquery' ], version( '/assets/js/build.js' ), true );
	wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', [ 'build' ], version( '/assets/js/main.js' ), true );
	wp_enqueue_script( 'valid-js', get_stylesheet_directory_uri() . '/assets/js/valid.js', [ 'build' ], version( '/assets/js/valid.js' ), true );
	wp_enqueue_script( 'sw_alert', '//cdn.jsdelivr.net/npm/sweetalert2@9', [ 'build' ], '', true );
	wp_enqueue_style( 'main-css', get_stylesheet_directory_uri() . '/assets/css/main.css', '', version( '/assets/css/main.css' ) );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800|Roboto:400,700&amp;display=swap', '' );
	wp_enqueue_style( 'sw_alert_css', '//cdn.jsdelivr.net/npm/sweetalert2@9.15.1/dist/sweetalert2.min.css', '' );
	
}


// add support svg
function add_file_types_to_uploads ( $file_types ) {
	$new_filetypes        = [];
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types           = array_merge( $file_types, $new_filetypes );
	
	return $file_types;
}

add_filter( 'upload_mimes', 'add_file_types_to_uploads', 50 );

// add version file
function version ( $src ) {
	return filemtime( get_stylesheet_directory() . $src );
}

add_action( 'after_setup_theme', 'remove_wc_action' );
function remove_wc_action () {
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices', 10 );
	remove_action( 'woocommerce_before_checkout_form', 'woocommerce_output_all_notices', 10 );
	remove_action( 'woocommerce_before_checkout_billing_form', 'woocommerce_output_all_notices', 10 );
	
	
	add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );
//	add_action('woocommerce_before_single_product_summary', 'woocommerce_breadcrumb', 6);
	
	// add custom data
	add_filter( 'woocommerce_add_cart_item_data', 'jwa_add_boost_to_cart_item', 10, 3 );
	add_filter( 'woocommerce_get_item_data', 'jwa_display_cart', 10, 2 );
	add_filter( 'woocommerce_before_calculate_totals', 'jwa_calculate_total_price', 10, 2 );
	
	
	if ( ! is_user_logged_in() ) {
		add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	}
	
	//add custom image size
	add_image_size( 'product_big_image', 740, 335, [ 'top', 'center' ], true );
}

function jwa_add_boost_to_cart_item ( $cart_item_data, $product_id, $variation_id ) {
	$boost_flag                       = filter_input( INPUT_POST, 'boost-' . $product_id );
	$boost_value                      = filter_input( INPUT_POST, 'boost_words-' . $product_id );
	$paymentMethod                    = filter_input( INPUT_POST, 'payment_method' );
	$cart_item_data['payment_method'] = $paymentMethod;
	
	if ( empty( $boost_flag ) ) {
		return $cart_item_data;
	}
	
	$cart_item_data['boost_flag'] = $boost_flag;
	
	$boost = get_field( 'boost', $product_id );
	foreach ( $boost as $item ) {
		if ( $item['price'] == $boost_value ) {
			$cart_item_data['boost_value'] = [ $item['value'] => $boost_value ];
		}
	}
	
	
	return $cart_item_data;
}

function jwa_display_cart ( $item_data, $cart_item ) {
	$item_data[] = [
		'key'     => __( 'Payment method', 'jwa' ),
		'value'   => wc_clean( $cart_item['payment_method'] ),
		'display' => '',
	];
	
	if ( empty( $cart_item['boost_flag'] || $cart_item['boost_value'] ) ) {
		return $item_data;
	}
	$item_data[] = [
		'key'     => __( 'Boost flag', 'jwa' ),
		'value'   => wc_clean( $cart_item['boost_flag'] ),
		'display' => '',
	];
	
	$item_data[] = [
		'key'     => __( 'Boost words', 'jwa' ),
		'value'   => wc_clean( key( $cart_item['boost_value'] ) ),
		'display' => '',
	];
	
	$item_data[] = [
		'key'     => __( 'Boost price', 'jwa' ),
		'value'   => wc_clean( array_values( $cart_item['boost_value'] )[0] ),
		'display' => '',
	];
	
	
	return $item_data;
}

function jwa_calculate_total_price ( $cart_object ) {
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
		return;
	}
	
	foreach ( $cart_object->get_cart() as $hash => $value ) {
		
		if ( $value["boost_flag"] == 'on' ) {
			$boostPrice    = (int) array_values( $value["boost_value"] )[0];
			$quantity      = (int) $value['quantity'];
			$standardPrice = $value['data']->get_regular_price();
			
			$boost = $boostPrice * $quantity;
			$price = $standardPrice * $quantity;
			
			$newPrice = $boost + $price;
			$value['data']->set_price( $newPrice );
		}

//		var_dump( $value['data'] );
		
	}
}

add_action( 'woocommerce_checkout_create_order_line_item', 'wdm_add_custom_order_line_item_meta', 10, 4 );

function wdm_add_custom_order_line_item_meta ( $item, $cart_item_key, $values, $order ) {
	
	$item->add_meta_data( 'Boost ', $values["boost_flag"] );
	$item->add_meta_data( 'Boost Words ', key( $values["boost_value"] ) );
	$item->add_meta_data( 'Boost Price ', array_values( $values["boost_value"] )[0] );
	$item->add_meta_data( 'Payment method', $values["payment_method"] );
	
}

add_filter( 'the_content', 'add_name_user', 10, 1 );

function add_name_user ( $conetnt ) {
	$userID   = get_current_user_id();
	$userName = get_user_meta( $userID )['nickname'][0];
	
	return str_replace( '{{name}}', $userName, $conetnt );
}

add_action( 'woocommerce_thankyou', 'redirection_to_question', 70, 1 );
function redirection_to_question ( $order ) {
	$_SESSION['order_id'] = $order;
	$page                 = get_page_by_path( 'question' );
	wp_safe_redirect( get_the_permalink( $page->ID ), '301' );
}

add_filter( 'wp_nav_menu_items', 'add_login_logout_link', 10, 2 );
function add_login_logout_link ( $items, $args ) {
	
	if ( $args->theme_location == 'main-menu' ) {
		if ( is_user_logged_in() ) {
			$loginoutlink = '<a href="' . wp_logout_url( get_bloginfo( 'url' ) ) . '">Log out</a>';
			$itemsNew     = '<li>' . $loginoutlink . '</li>';
			
			return $itemsNew;
		} else {
			$loginoutlink = '<a href="' . get_bloginfo( 'url' ) . '/login">Log in</a>';
		}
		
		$itemsNew = '<li>' . $loginoutlink . '</li>';
		
		return $itemsNew .= $items;
	} else {
		return $items;
	}
	
}


// add ajax question to order
require_once dirname( __FILE__ ) . '/include/ajax/add_question_order.php';

//add registration ajax
require_once dirname( __FILE__ ) . '/include/ajax/jwa_registration.php';

// jwa login form
require_once dirname( __FILE__ ) . '/include/ajax/login/login.php';

// add meta box
require_once dirname( __FILE__ ) . '/include/meta_box/question_order/question_order.php';
