<?php
/**
 * Created 17.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action( 'add_meta_boxes', 'jwa_add_question_box' );
function jwa_add_question_box () {
	$screens = [ 'shop_order' ];
	add_meta_box( 'jwa_question_meta_box', 'Buyer Answers', 'jwa_meta_box_callback', $screens );
}

function jwa_meta_box_callback ( $post, $meta ) {
	$screens = $meta['args'];
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
	
	
	$data = unserialize( get_post_meta( $post->ID, 'jwa_question', 1 ) );
	$html = '';
	foreach ( $data as $item ) {
		$html .= '<ul>';
		if ( is_array( $item ) && $item != '' ) {
			foreach ( $item as $key => $i ) {
				$html .= '<li>';
				$html .= $key . ': ';
				$html .= $i;
				$html .= '</li>';
			}
		}
		$html .= '</ul>';
		$html .= '<br>';
	}
	
	echo $html;
}

