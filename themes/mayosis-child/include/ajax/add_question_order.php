<?php
/**
 * Created 17.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action( 'wp_ajax_add_question_order', 'add_question_order' );
add_action( 'wp_ajax_nopriv_add_question_order', 'add_question_order' );

function add_question_order () {
	$field   = $_POST['filed'];
	$orderID = $_POST['orderID'];
//	var_dump( $_POST['filed'] );
	$res = add_post_meta( $orderID, 'jwa_question', serialize( $field ), true );
	
	wp_send_json_success( [ 'res' => $res ] );
}