<?php
/**
 * Created 23.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

add_action( 'wp_ajax_jwa_registration', 'jwa_registration' );
add_action( 'wp_ajax_nopriv_jwa_registration', 'jwa_registration' );

function jwa_registration () {
	$login = $_POST['login'];
	$email = $_POST['email'];
	if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		wp_send_json_error( [ 'message' => 'Didn’t get a valid email', 'code' => '202' ] );
	}
	
	if ( is_user_logged_in() ) {
		wp_send_json_error( [
			'message'  => 'User is already logged in',
			'code'     => '101',
			'redirect' => get_bloginfo( 'url' ),
		] );
	}
	
	if ( empty( $login ) ) {
		wp_send_json_error( [
			'message' => 'User name is empty',
			'code'    => '102',
		] );
	}
	
	$password = wp_generate_password( 8, true, false );
	
	$userdata = [
		'user_pass'     => $password,
		'user_login'    => $email,
		'user_nicename' => $login,
		'user_email'    => $email,
		'role'          => 'subscriber',
		'first_name'    => $login,
	];
	
	$user_id = wp_insert_user( $userdata );
	
	if ( ! is_wp_error( $user_id ) ) {
		
		$to      = $email;
		$subject = 'Thank you for joining us.';
		$message = '
<html>
<head>
  <title>Thank you for joining us. SEO Concierge</title>
</head>
<body>
  <p>You have successfully registered, please click on the link</p>
  <p>Login: ' . $login . '</p>
  <p>Password: ' . $password . '</p>
  <a href="' . get_bloginfo( 'url' ) . '/login">Login</a>
</body>
</html>
';
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		
		$send = mail( $to, $subject, $message, $headers );
		
		wp_send_json_success( [ 'userID' => $user_id, 'mailsend' => $send, 'redirect' => get_bloginfo( 'url' ) ] );
	} else {
		wp_send_json_error( [ 'message' => $user_id->get_error_message(), 'code' => '201' ] );
	}
}
