<?php
/**
 * Created 29.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

add_action( 'wp_ajax_jwa_login', 'jwa_login' );
add_action( 'wp_ajax_nopriv_jwa_login', 'jwa_login' );

function jwa_login () {
	$email    = $_POST['email'];
	$psw      = $_POST['password'];
	$redirect = $_POST['redirectTo'];
	
	if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
		wp_send_json_error( [ 'message' => 'Didn’t get a valid email', 'code' => '202' ] );
	}
	
	if ( is_user_logged_in() ) {
		wp_send_json_error( [ 'message' => 'User is already logged in', 'code' => '101', 'redirect' => $redirect ] );
	}
	
	
	$user = wp_signon( [
		'user_login'    => $email,
		'user_password' => $psw,
		'remember'      => 1,
	] );
	
	if ( is_wp_error( $user ) ) {
		wp_send_json_error( [ 'message' => $user->get_error_message(), 'code' => '201' ] );
	}
	
	wp_send_json_success( [ 'redirect' => $redirect ] );
}
