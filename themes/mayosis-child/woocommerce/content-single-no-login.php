<?php
/**
 * Created 13.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	
	return;
}


$url = add_query_arg(
	'redirect_to',
	get_permalink( get_the_ID() ),
	site_url( '/my-account/' ) // your my acount url
);

?>
<div class="title" style="background-color:#F5F5F5;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php do_action( 'woocommerce_before_single_product_summary' ); ?>
			</div>
		</div>
	</div>
</div>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'product-inner', $product ); ?>>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="banner">
					<?php if ( has_post_thumbnail( get_the_ID() ) ): ?>
						<?php the_post_thumbnail( 'product_big_image' ); ?>
					<?php else: ?>
						<img src="https://via.placeholder.com/740x334" alt="No Image">
					<?php endif; ?>
					<div class="price">
						<h3>Starting from<span>$<?php echo $product->get_price(); ?></span></h3>
						<a class="icon-right" href="<?php echo bloginfo( 'url' ) ?>/login">Get Started</a>
					</div>
				</div>
				<div class="description">
					<?php the_content(); ?>
					<a class="button icon-right" href="<?php echo bloginfo( 'url' ) ?>/registration">Get Started</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!--<div class="recommended" style="background-color:#F5F5F5;">-->
<!--	<div class="container">-->
<!--		<div class="row">-->
<!--			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
<!--				--><?php //do_action( 'woocommerce_after_single_product_summary' );?>
<!--		</div>-->
<!--	</div>-->
<!--</div>-->
