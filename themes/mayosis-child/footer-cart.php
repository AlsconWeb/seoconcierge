<?php
/**
 * Created 19.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 */

?>
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package mayosis
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<?php wp_footer(); ?>
</body>
<!-- End Main Layout -->
</html>
