<?php
/**
 * Created 13.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Template Name: Login
 */

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Favicon -->
	<?php
	
	global $edd_options;
	$favicon       = get_theme_mod( 'favicon-upload' );
	$loaderwebsite = get_theme_mod( 'loader_website', 'hide' );
	
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		if ( ! empty( $favicon ) ) {
			?>
			<link rel="shortcut icon" href="<?php echo esc_url( $favicon ); ?>" type="image/x-icon"/>
			<?php
		} else {
			?>
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/fav.png" type="image/x-icon">
			<?php
		}
	}
	?>
	<?php wp_head(); ?>
</head>
<body>
<div class="login"
     style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/clouds.png), url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/rocket.svg)">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<form name="loginform" id="jwa_login"
				      method="post">
					<a class="logo" href="<?php bloginfo( 'url' ); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.svg" alt="logo"></a>
					<p class="error" style="display: none;"></p>
					<div class="input">
						<label>Email...</label>
						<input type="text" id="user_login" name="log" autocapitalize="off"
						       onfocus="this.removeAttribute('readonly');" readonly autocomplete="off">
					</div>
					<div class="input">
						<label>Password...</label>
						<input type="password" name="pwd" id="user_pass" onfocus="this.removeAttribute('readonly');" readonly
						       autocomplete="off">
					</div>
					<div class="input submit">
						<input class="button" type="submit" value="Log In" name="wp-submit" id="wp-submit">
						<input type="hidden" name="redirect_to" value="<?php echo get_bloginfo( 'url' ); ?>">
						<input type="hidden" name="testcookie" value="1">
					</div>
					<a href="<?php bloginfo( 'url' ); ?>/registration">Dont have an account?</a>
					<ul>
						<li><a href="<?php bloginfo( 'url' ); ?>">Home</a></li>
						<li><a href="<?php bloginfo( 'url' ); ?>/forgot-password/">Forgot your password? </a></li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	var homeURL = "<?php bloginfo( 'url' );?>";
</script>
<?php wp_footer(); ?>
</body>
</html>
