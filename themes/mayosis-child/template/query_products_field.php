<?php
/**
 * Created 17.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Template Name: Query Products Field
 */

$orderID     = $_SESSION['order_id'];
$order       = new WC_Order( $orderID );
$order_items = $order->get_items();
$j           = 0;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Question</title>
	<?php wp_head(); ?>
</head>
<body>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm">
				<h1><?php the_title(); ?></h1>
				<span class="order" data-order="<?php echo $orderID; ?>"></span>
				<?php the_content(); ?>
				<form class="form-question" method="post">
					<div class="panel-group" id="accordion">
						<?php foreach ( $order_items as $item_id => $item ): ?>
							<?php
							$product  = $item->get_product();
							$quantity = (int) $item['qty'];
							
							$tag = get_the_terms( $item['product_id'], 'product_tag' )[0]->name; ?>
							
							<?php switch ( $tag ):
								case 'audit': ?>
									<?php for ( $i = 0; $i < $quantity; $i ++ ): ?>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
													   href="#collapse-<?php echo $j . '-' . $i; ?>"><?php echo $product->name; ?>
														- <?php echo $i + 1; ?></a>
												</h4>
											</div>
											<div id="collapse-<?php echo $j . '-' . $i; ?>"
											     class="panel-collapse collapse <?php echo $j == 0 ? 'in' : '' ?>">
												<div class="panel-body">
													<div class="field" data-item="<?php echo $i + 1; ?>">
														<span data-product="<?php echo $product->name; ?>"></span>
														<div class="input">
															<label for="url-<?php echo $j . '-' . $i; ?>">Enter Url </label>
															<input class="url" type="text" name="url" id="url-<?php echo $j . '-' . $i; ?>" required>
														</div>
														<div class="textarea">
															<label for="comment-<?php echo $j . '-' . $i; ?>">Enter Comment </label>
															<textarea class="comment" name="comment" id="comment-<?php echo $j . '-' . $i; ?>"
															          cols="30" rows="10"
															          required></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endfor; ?>
									<?php break; ?>
								<?php case 'backlinks': ?>
									<?php for ( $i = 0; $i < $quantity; $i ++ ): ?>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
													   href="#collapse-<?php echo $j . '-' . $i; ?>"><?php echo $product->name; ?>
														- <?php echo $i + 1; ?></a>
												</h4>
											</div>
											<div id="collapse-<?php echo $j . '-' . $i; ?>"
											     class="panel-collapse collapse <?php echo $j == 0 ? 'in' : '' ?>">
												<div class="panel-body">
													<div class="field" data-item="<?php echo $i + 1; ?>">
														<span data-product="<?php echo $product->name; ?>"></span>
														<div class="input">
															<label for="url-<?php echo $j . '-' . $i; ?>">Enter Url </label>
															<input class="url" type="text" name="url" id="url-<?php echo $j . '-' . $i; ?>" required>
														</div>
														<div class="textarea">
															<label for="anchors-<?php echo $j . '-' . $i; ?>">Enter Anchors </label>
															<textarea name="anchors" id="anchors-<?php echo $j . '-' . $i; ?>" cols="10" rows="4"
															          required></textarea>
														</div>
														<div class="textarea">
															<label for="comment-<?php echo $j . '-' . $i; ?>">Enter Comment </label>
															<textarea class="comment" name="comment" id="comment-<?php echo $j . '-' . $i; ?>"
															          cols="30" rows="10"
															          required></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endfor; ?>
									<?php break; ?>
								<?php case 'content': ?>
									<?php for ( $i = 0; $i < $quantity; $i ++ ): ?>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
													   href="#collapse-<?php echo $j . '-' . $i; ?>"> <?php echo $product->name; ?> - <?php
														echo $i + 1; ?></a>
												</h4>
											</div>
											<div id="collapse-<?php echo $j . '-' . $i; ?>"
											     class="panel-collapse collapse <?php echo $j == 0 ? 'in' : '' ?>">
												<div class="panel-body">
													<div class="field" data-item="<?php echo $i + 1; ?>">
														<span data-product="<?php echo $product->name; ?>"></span>
														<h4>Product Name: <?php echo $product->name; ?> - <?php echo $i + 1; ?></h4>
														<div class="input">
															<label for="keyword-<?php echo $j . '-' . $i; ?>">Enter main keyword <span>separate the words , </span>
															</label>
															<input type="text" name="keyword" id="keyword-<?php echo $j . '-' . $i; ?>" required>
														</div>
														<div class="textarea">
															<label for="a_keywords-<?php echo $j . '-' . $i; ?>">Enter additional keywords
																<span>separate the words , </span></label>
															<textarea name="a_keywords" id="a_keywords-<?php echo $j . '-' . $i; ?>" cols="10"
															          rows="4" required></textarea>
														</div>
														<div class="textarea">
															<label for="comment-<?php echo $j . '-' . $i; ?>">Enter Comment</label>
															<textarea class="comment" name="comment" id="comment-<?php echo $j . '-' . $i; ?>"
															          cols="30"
															          rows="10"
															          required></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endfor; ?>
									<?php break; ?>
								<?php endswitch;
							$j ++; ?>
						<?php endforeach; ?>
					</div>
					<input type="submit" value="Send" class="btn">
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	var homeUrl = "<?php bloginfo( 'url' );?>";
</script>
<?php wp_footer(); ?>
</body>
</html>

