<?php
/**
 * Created 14.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Template Name: Registration
 */

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Favicon -->
	<?php
	global $edd_options;
	$favicon       = get_theme_mod( 'favicon-upload' );
	$loaderwebsite = get_theme_mod( 'loader_website', 'hide' );
	
	if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
		if ( ! empty( $favicon ) ) {
			?>
			<link rel="shortcut icon" href="<?php echo esc_url( $favicon ); ?>" type="image/x-icon"/>
			<?php
		} else {
			?>
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/fav.png" type="image/x-icon">
			<?php
		}
	}
	?>
	<?php wp_head(); ?>
</head>
<body>
<div class="login"
     style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/clouds.png), url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/rocket.svg)">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form name="registerform" id="registerform">
					<a class="logo" href="<?php bloginfo( 'url' ); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.svg" alt="logo"></a>
					
					<p class="error" style="display: none;"></p>
					<div class="input">
						<label>Name...</label>
						<input type="text" name="user_login" id="user_login" autocapitalize="off"
						       onfocus="this.removeAttribute('readonly');" readonly autocomplete="off" required>
					</div>
					<div class="input">
						<label>Email...</label>
						<input type="email" name="user_email" id="user_email" onfocus="this.removeAttribute('readonly');" readonly
						       autocomplete="off" required>
					</div>
					<div class="input">
						<p id="reg_passmail">Registration confirmation will be emailed to you.</p>
						<input type="hidden" name="redirect_to" value="">
					</div>
					<div class="input submit">
						<input class="button" type="submit" value="Sign Up" name="wp-submit" id="wp-submit">
					</div>
					<a href="<?php bloginfo( 'url' ); ?>/login">I'm have an account?</a>
					<ul>
						<li><a href="<?php bloginfo( 'url' ); ?>">Go to Home</a></li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>

