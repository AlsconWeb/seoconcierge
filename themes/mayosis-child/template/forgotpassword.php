<?php
/**
 * Created 15.04.2020
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Template Name: Forgot Password
 */

get_header();
?>
<div class="login"
     style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/clouds.png), url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/rocket.svg)">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<form name="lostpasswordform" id="lostpasswordform"
				      action="https://thompson.justwebagency.com/gsklaw/wp-login.php?action=lostpassword" method="post">
					<a class="logo" href="<?php bloginfo( 'url' ); ?>">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.svg" alt="logo">
					</a>
					<p>Please enter your username or email address. You will receive a link to create a new password via
						email.</p>
					<div class="input">
						<label>Username or Email Address</label>
						<input type="text" name="user_login" id="user_login" autocapitalize="off"
						       onfocus="this.removeAttribute('readonly');" readonly autocomplete="off" required>
					</div>
					<div class="input submit">
						<input type="hidden" name="redirect_to" value="">
						<input class="button" type="submit" value="Get New Password" name="wp-submit" id="wp-submit">
					</div>
					<a href="<?php bloginfo( 'url' ); ?>/login">Already Have An Account?</a>
					<ul>
						<li><a href="<?php bloginfo( 'url' ); ?>">Home</a></li>
					</ul>
				</form>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
