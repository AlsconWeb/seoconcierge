jQuery(document).ready(($) => {
	
	let totalPrice = 0;
	totalPricePrint($('.woocommerce-grouped-product-list-item'));
	$(".qty").change(function (e) {
		totalPricePrint($('.woocommerce-grouped-product-list-item'));
	});
	
	$(".woocommerce-grouped-product-list-item__quantity .increment, .woocommerce-grouped-product-list-item__quantity .decrement ").click(function (e) {
		totalPricePrint($('.woocommerce-grouped-product-list-item'));
	});
	
	$("input[name^=boost-]").change(function (e) {
		totalPricePrint($('.woocommerce-grouped-product-list-item'));
	});
	
	$(".select select[name^=boost_words]").change(function (e) {
		totalPricePrint($('.woocommerce-grouped-product-list-item'));
	});
	
	
	function totalPricePrint(product) {
		let totalPrice = 0;
		let totalBoost = 0;
		let priceText = $('.details p.price span');
		
		$(product).each(function (i, item) {
			let count = $(this).find('.qty').val();
			let priceProd = $(this).find('.woocommerce-grouped-product-list-item__price ins .woocommerce-Price-amount.amount').text();
			// console.log('price', priceProd, 'this', $(this))
			priceProd = Number(priceProd.replace('$', '').split(' ')[0]);
			let boostFlag = $(this).find('input[name^=boost-]:checked');
			let boostPrice = $(this).find('.woocommerce-grouped-product-list-item__boost_select select[name^=boost_words-]');
			
			if (count != 0 || count != '0') {
				totalPrice = totalPrice + (priceProd * count);
			}
			
			if (boostFlag.length > 0) {
				if ($(boostPrice).prop("selected", true)) {
					totalBoost = totalBoost + (boostPrice.val() * count);
				}
			}
			
			// console.log('totalPrice', totalPrice, totalBoost);
			priceText.text(String('$' + Number(totalPrice + totalBoost)));
		});
	}
	
	
	$(".form-question").submit(function (e) {
		e.preventDefault();
		let fields = $('.field');
		let fieldArr = new Array();
		$(fields).each(function (i, item) {
			let url = $(this).find('input[name=url]').val(),
				comment = $(this).find('textarea[name=comment]').val(),
				anchors = $(this).find('textarea[name=anchors]').val(),
				keyword = $(this).find('input[name=keyword]').val(),
				count = $(this).data('item'),
				a_keywords = $(this).find('textarea[name=a_keywords]').val();
			
			if (anchors == '') {
				anchors = ''
			}
			if (keyword == '') {
				keyword = ''
			}
			if (a_keywords == '') {
				a_keywords = ''
			}
			
			
			let itemName = $(this).find('span').data('product');
			
			fieldArr[i] = {
				url: url,
				comment: comment,
				anchors: anchors,
				keyword: keyword,
				additional_keywords: a_keywords,
				name: itemName
			};
		});
		
		let data = {
			action: 'add_question_order',
			filed: fieldArr,
			orderID: $('.order').data('order'),
		}
		console.log(data);
		
		$.ajax({
			type: "post",
			url: wc_add_to_cart_params.ajax_url,
			data: data,
			beforeSend: () => {
			},
			success: function (res) {
				console.log(res);
				if (res.data.res == false) {
					alert('Error: Your request has not been processed.')
				} else {
					location.href = homeUrl + '/';
				}
			}
		});
	});
	
	$("#registerform").submit(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'jwa_registration',
			login: $('#user_login').val(),
			email: $('#user_email').val(),
		}
		
		$.ajax({
			type: "post",
			url: wc_add_to_cart_params.ajax_url,
			data: data,
			beforeSend: () => {
			},
			success: function (res) {
				console.log(res.data);
				if (res.success) {
					Swal.fire({
						title: 'Thanks You',
						text: "We have sent you an email with access",
						icon: 'success',
						confirmButtonText: 'Ok'
					}).then((result) => {
						location.href = res.data.redirect;
					})
				} else {
					console.log('swi', res.data.code)
					switch (res.data.code) {
						case '101':
							$('p.error').text(res.data.message).hide();
							location.href = res.data.redirect;
							break;
						case '102':
							$('p.error').text(res.data.message).show();
							break;
						case '201':
							$('p.error').text(res.data.message).show()
							break;
						case '202':
							$('input[name=log]').css({'border': "1px solid red !important;"});
							$('p.error').text(res.data.message).show();
							break;
					}
				}
			}
		});
	});
	
	if ($('input[name=redirect_to]').length > 0) {
		
		$('input[name=redirect_to]').attr('value',);
	}
	
	$("#jwa_login").submit(function (e) {
		e.preventDefault();
		
		let data = {
			action: 'jwa_login',
			email: $(this).find('input[name=log]').val(),
			password: $(this).find('input[name=pwd]').val(),
			redirectTo: $(this).find('input[name=redirect_to]').val(),
		};
		
		$.ajax({
			type: 'POST',
			url: wc_add_to_cart_params.ajax_url,
			data: data,
			success: function (res) {
				console.log(res);
				if (res.success) {
					location.href = res.data.redirect;
				} else {
					switch (res.data.code) {
						case '101':
							$('p.error').text(res.data.message).hide();
							location.href = homeURL;
							break;
						case '201':
							$('p.error').text(res.data.message)
							break;
						case '202':
							$('input[name=log]').css({'border': "1px solid red !important;"});
							$('p.error').text(res.data.message).show();
							break;
					}
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
		
	});
	
});
