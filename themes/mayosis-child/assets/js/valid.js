jQuery(document).ready(function($){
	var valid = false;
	function valids() {
		$('.url').each(function(){
			var url = $(this);
				url.change(function(){
				var input = this.value;
				var regExpUrl = new RegExp("(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?((\\w|-)+)(([.]|[/])((\\w|-)+))+"); 
				if (input && regExpUrl.test(input)) {
					$(this).parent().find("p").remove();
					$(this).parents('.input').addClass('success');
					$(this).parents('.input').removeClass('incorrect');
					valid= true;
					console.log('valid', valid);
				} else {
					if($(this).parent().find('.error').length == 0){
						$(this).parent().append('<p class="error">Please, fill in the url...</p>')
					}
					$(this).parents('.input').removeClass('success');
					$(this).parents('.input').addClass('incorrect');
					valid= false;
					console.log('valid', valid);
				}
			});
		});
		$('[name="comment"]').each(function(){
			var comment = $(this);
			comment.change(function(){
				console.log($(this).val().length)
				if($(this).val().length >= 1){
					$(this).parent().find("p").remove();
					$(this).parents('.textarea').addClass('success');
					$(this).parents('.textarea').removeClass('incorrect');
					valid= true;
					console.log('valid', valid);
				}else{
					if($(this).parent().find('.error').length == 0){
						$(this).parent().append('<p class="error">Please, fill in the comment...</p>')
					}
					$(this).parents('.textarea').removeClass('success');
					$(this).parents('.textarea').addClass('incorrect'); 
					valid= false;
					console.log('valid', valid);
				}
			});
		});
		$('[name="anchors"]').each(function(){
			var anchors = $(this);
			anchors.change(function(){
				console.log($(this).val().length)
				if($(this).val().length >= 1){
					$(this).parent().find("p").remove();
					$(this).parents('.textarea').addClass('success');
					$(this).parents('.textarea').removeClass('incorrect');
					valid= true;
					console.log('valid', valid);
				}else{
					if($(this).parent().find('.error').length == 0){
						$(this).parent().append('<p class="error">Please, fill in the anchors...</p>')
					}
					$(this).parents('.textarea').removeClass('success');
					$(this).parents('.textarea').addClass('incorrect');
					valid= false;
					console.log('valid', valid);
				}
			});
		});
		return valid;
	}
	valids();
	
	$('.url, [name="anchors"], [name="comment"]').change(function(){
		valids();
		var lengthEl = $(this).parents('.field').find('div').length
		console.log(lengthEl) 
		if($(this).parents('.field').find('.success').length == lengthEl){
			$(this).parents('.panel').addClass('success')
			console.log('if')
		}else{
			console.log('else')
			$(this).parents('.panel').removeClass('success')
		}
	});

	

});